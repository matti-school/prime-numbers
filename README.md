# Schulprojektprojekt zum finden von Primzahlen

Diese Anwendung wird im Rahmen der Ausbildung zum Fachinformatiker für Anwendungsentwicklung verfasst.
Sie dient als Grundprojekt zum Lernen, Üben und ausprobieren, im Rahmen der Ausbildung.

## Voraussetzungen

Im Rahmen des Berufsschulunterrichts soll ein Programm erstellt werden, welches alle Primzahlen zwischen 1 und 1Mio. findet.

Performance ist hierbei Key. Das Programm muss schneller als 1 sec sein.

Möchte man den Code Lokal ausführen so sind folgende Dinge von nöten:

    - GIT zum pullen der Anwendung
    - entsprechende Berechtigungen zum pullen
    - gcc compiler

## Lokal einrichten und Starten

Folgende befehle, sind in der Bash console auszuführen:

```bash
git clone git@gitlab.com:matti-school/prime-numbers.git

cd prime-numbers  #oder jeweiligen geänderten verzeichnis Namen

make -f prime_src.m
```

Um das Programm zu Starten gibt man folgendes ein:

```bash
./runThis

# or

time ./runThis
```

## Zum Projekt als Tester beitragen

Ich heiße Feedback und konstruktive Kritik jederzeit willkommen.

Feedback bzw. Kritik kann sowohl als DM als auch ober das Issue Feature von Gitlab eingereicht werden.