#include <stdio.h>

int isPrime(int num) {
    for (int i = 2; i * i <= num; i++) {
        if (num % i == 0) {
            return 0;
        }
    }
    return 1;
}

int main() {
    int lowerBound = 2;
    int upperBound = 1000000;

    for (int i = lowerBound; i <= upperBound; i++) {
        if (isPrime(i)) {
            printf("%d, ", i);
        }
    }

    return 0;
}